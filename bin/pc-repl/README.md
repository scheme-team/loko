This is a graphical REPL that can run in emulators (VESA graphics
coming any day now).

Building the disk image requires mtools.

When it's all said and done, you get a REPL running on a video
framebuffer and you should be able to import and use the libraries
from `.akku/lib`.

It is very rough in the edges but should work.
