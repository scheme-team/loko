;; -*- mode: scheme; coding: utf-8 -*-
;; SPDX-License-Identifier: AGPL-3.0-or-later
;; Loko Scheme - an R6RS Scheme compiler
;; Copyright © 2019-2020 Göran Weinholt

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.
#!r6rs

;;;

(library (loko runtime scheduler)
  (export
    ;; Memory handling
    dma-allocate
    dma-free

    ;; IRQ/signal handling
    enable-irq
    disable-irq
    acknowledge-irq
    wait-irq-operation

    ;; POSIX signals
    signal-signal!
    enable-signal
    acknowledge-signal
    wait-signal-operation

    ;; Misc calls to the scheduler
    get-environment
    get-pid
    get-boot-loader
    get-boot-modules
    get-command-line
    scheduler-wait
    new-process
    process-exit

    ;; Process identifiers
    pid? pid-value

    ;; XXX: Stands to be cleaned up
    pc-current-ticks

    ;; Scheduler
    scheduler-loop)
  (import
    (rnrs (6))
    (loko match)
    (loko system $primitives)
    (loko runtime buddy)
    (loko runtime fibers)
    (except (loko system $host) dma-allocate dma-free
            enable-irq disable-irq acknowledge-irq wait-irq-operation
            signal-signal! enable-signal acknowledge-signal
            wait-signal-operation)
    (only (loko runtime context) CPU-VECTOR:SCHEDULER-RUNNING?
          CPU-VECTOR:SCHEDULER-SP)
    (prefix (srfi :98 os-environment-variables) srfi-98:))

(define ($process-yield msg)
  (let ((sched-sp ($processor-data-ref CPU-VECTOR:SCHEDULER-SP)))
    ;; sched-sp is 0 if the scheduler is running
    ;; (display "Yielding back to SCHED-SP=")
    ;; (display (number->string sched-sp 16))
    ;; (newline)
    (when (eqv? sched-sp 0)
      (error '$process-yield "The scheduler tried to yield"))
    ;; IRQs should be disabled when the scheduler is running
    ($disable-interrupts)
    ($processor-data-set! CPU-VECTOR:SCHEDULER-RUNNING? #t) ;currently yielding
    (let ((msg ($switch-stack sched-sp msg)))
      ($processor-data-set! CPU-VECTOR:SCHEDULER-RUNNING? #f) ;no longer yielding
      ($enable-interrupts)    ;FIXME: should not be used under Linux
      ;; (display "Secret code from scheduler: ")
      ;; (write msg)
      ;; (newline)
      msg)))

(define-record-type pid
  (sealed #t) (opaque #f)
  (fields value))

(define (get-command-line)
  (let ((cmdline ($process-yield '(command-line))))
    (assert (list? cmdline))
    (map string-copy cmdline)))

(define (get-environment)
  (let ((env ($process-yield '(environment))))
    (assert (list? env))
    (map (lambda (var)
           (cons (string-copy (car var))
                 (string-copy (cdr var))))
         env)))

(define (get-boot-modules)
  (let ((modules ($process-yield '(boot-modules))))
    (assert (list? modules))
    (map (match-lambda
          [((? string? fn) _args (? fixnum? start) (? fixnum? len))
           (list (string-copy fn) '() start len)])
         modules)))

(define (get-pid)
  (let ((id ($process-yield '(get-pid))))
    (assert (fixnum? id))
    (make-pid id)))

(define (get-boot-loader)
  ($process-yield '(boot-loader)))

(define (new-process)
  (let ((status ($process-yield (vector 'new-process #f))))
    (assert (eq? status 'ok))
    (make-pid (vector-ref status 1))))

(define (process-exit status)
  ($process-yield `(exit ,(pid-value (get-pid))
                         ,status)))

(define (pc-current-ticks)
  ($process-yield '(current-ticks)))

(define (dma-allocate size mask)
  (assert (fixnum? size))
  (assert (fixnum? mask))             ;#xfffff000 is a nice mask
  (let* ((v `#(allocate ,size ,mask #f))
         (s ($process-yield v)))
    (unless (eq? s 'ok)
      (error 'dma-allocate "Memory allocation failed" size mask))
    (let ((cpu-addr (vector-ref v 3)))
      cpu-addr)))

(define (dma-free addr)
  (assert (fixnum? addr))
  (let* ((v `#(free ,addr))
         (s ($process-yield v)))
    (unless (eq? s 'ok)
      (error 'dma-free "Memory release failed" addr))))

(define (scheduler-wait ns-timeout)
  ;; TODO: this message should take a number back from the scheduler
  ;; that indicates for how long it slept. This is so that if the
  ;; process is awoken by an uninteresting message it can go back to
  ;; sleeping with the original timeout without asking the scheduler
  ;; for the current time.
  (when ns-timeout
    (assert (fxpositive? ns-timeout)))
  (let ((vec (vector 'wait ns-timeout #f)))
    (handle-scheduler-wait-reply vec ($process-yield vec)
                                 'scheduler-wait)))

;;; IRQ support

(define *interrupt-cvars* (make-vector 256 #f))

(define (handle-scheduler-wait-reply vec reply who)
  ;; The scheduler will update vec.
  (case reply
    ((timeout)
     #f)
    ((message)
     (let ((msg (vector-ref vec 2)))
       ;; This is currently just IRQ vector numbers, but can be used to
       ;; implement message passing between processes.
       (cond ((and (fixnum? msg) (vector-ref *interrupt-cvars* msg))
              => signal-cvar!))))
    (else
     (error who "Unknown reply from scheduler" reply))))

(define (enable-irq irq)
  (assert (fx<=? 0 irq 15))
  (unless (vector-ref *interrupt-cvars* irq)
    (vector-set! *interrupt-cvars* irq (make-cvar)))
  ($process-yield `#(enable-irq ,irq)))

(define (disable-irq irq)
  (assert (fx<=? 0 irq 15))
  ($process-yield `#(disable-irq ,irq)))

(define (acknowledge-irq irq)
  (define timeout 0)
  (assert (fx<=? 0 irq 15))
  (when timeout
    (assert (not (fxnegative? timeout))))
  (let ((vec (vector 'wait timeout #f)))
    (vector-set! *interrupt-cvars* irq (make-cvar))
    (handle-scheduler-wait-reply vec ($process-yield `#(acknowledge-irq ,irq ,vec))
                                 'acknowledge-irq)))

(define (wait-irq-operation irq)
  (wait-operation (vector-ref *interrupt-cvars* irq)))

;;; POSIX signal support

(define *signal-cvars* *interrupt-cvars*)

(define (signal-signal! signo)
  (cond ((vector-ref *signal-cvars* signo) => signal-cvar!)))

(define (enable-signal signo)
  (unless (vector-ref *signal-cvars* signo)
    (vector-set! *signal-cvars* signo (make-cvar))))

(define (acknowledge-signal signo)
  (unless (vector-ref *signal-cvars* signo)
    (error 'acknowledge-signal "Signal not enabled" signo))
  (vector-set! *interrupt-cvars* signo (make-cvar)))

(define (wait-signal-operation signo)
  (wait-operation (vector-ref *signal-cvars* signo)))

;;; This is an actual scheduler implementation, pid 0

;; This is pid 0 on Linux and NetBSD. It is not a real scheduler at
;; this point.

(define (scheduler-loop kernel buddies)
  ;; TODO: if msg is 'preempted then SIGURG delivery has been
  ;; disabled. When scheduling a process that was preempted there
  ;; is no need to manually unmask SIGURG. But if SIGURG is
  ;; blocked and we're scheduling a process that yielded or that
  ;; has not been started yet it is necessary to unmask SIGURG. If
  ;; SIGURG is unmasked and we're returning to a process that was
  ;; preempted it is likely necessary to mask SIGURG first, to
  ;; prevent a race condition. See sigprocmask.

  ;; TODO: most of the messages should have an implementation in
  ;; common with the one in pc-init, and probably the basic scheduling
  ;; as well.

  ;; TODO: more than one process

  ;; TODO: use epoll/kqueue here to do the equivalent of IRQs and HLT,
  ;; to allow fiber schedulers to exist in multiple processes

  (let ((sp* ($process-start 1))
        (m* #f)
        (pid* 1)
        (will-unmask?* #f)
        (URG-masked #f))
    (define-syntax print
      (syntax-rules ()
        #;
        ((_ args ...) (begin (display args) ... (newline)))
        ((_ . args) (begin 'dummy))))
    (let lp ((sp* sp*))
      (print "In scheduler! SP=" (number->string sp* 16))
      ;; Switch back to the process
      (let ((msg ($switch-stack sp* m*)))
        (print "Message: " msg)
        (cond ((eq? msg 'preempted)
               ;; The SIGURG signal handler preempted the process,
               ;; so SIGURG is now masked.
               (set! URG-masked #t)
               (set! will-unmask?* #t))
              (else
               ;; The process yielded, which means SIGURG is
               ;; unmasked.
               (set! URG-masked #f)
               (set! will-unmask?* #f)))
        (when (pair? msg)
          (case (car msg)
            ((exit)
             (let ((target (cadr msg))
                   (reason (caddr msg)))
               ;; XXX: Can't use conditions as reason because they are
               ;; generative.
               (exit (if (fixnum? reason) reason (eqv? reason #t)))))
            ((new-process)
             ;; TODO:
             (set! m* 'ok))
            ((boot-loader)
             (set! m* kernel))
            ((command-line)
             ;; XXX: command-line and environment are extremely iffy.
             ;; Process must immediately copy the variables to its own
             ;; storage, before we do a GC here.
             (set! m* (command-line)))
            ((environment)
             (set! m* (srfi-98:get-environment-variables)))
            ((get-pid)
             (set! m* pid*))))
        (when (vector? msg)
          (case (vector-ref msg 0)
            ((allocate)
             (let ((size (vector-ref msg 1))
                   (mask (vector-ref msg 2)))
               ;; Allocate a consecutive memory region.
               (cond ((buddies-allocate! buddies size mask) =>
                      (lambda (addr)
                        (vector-set! msg 3 addr)
                        (set! m* 'ok)))
                     (else
                      (set! m* #f)))))
            ((free)
             (let ((addr (vector-ref msg 1)))
               ;; Deallocate a previously allocated region.
               (cond ((buddies-free! buddies addr) =>
                      (lambda (addr)
                        (set! m* 'ok)))
                     (else
                      (set! m* #f))))))))
      (let ((sp ($processor-data-ref CPU-VECTOR:SCHEDULER-SP)))
        ($processor-data-set! CPU-VECTOR:SCHEDULER-SP 0)  ;indicate scheduler is running
        (lp sp))))))
