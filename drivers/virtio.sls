;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright © 2020 Göran Weinholt <goran@weinholt.se>
;; SPDX-License-Identifier: AGPL-3.0-or-later
#!r6rs

;;; Virtio transport help library

(library (loko drivers virtio)
  (export
    virtio-allocate-virtq
    virtq? virtq-free!

    virtq-num-free
    virtq-add-buffers!
    virtq-get-buffer
    virtq-enable-interrupts!
    virtq-disable-interrupts!
    virtq-kick

    ;; FIXME: The API is not sufficiently abstracted here
    virtio-device-status-set!
    virtio-isr-status!
    virtio-negotiate
    virtio-get-config-u8
    virtio-get-config-u16

    ;; virtio-device-status bitwise-ior'd flags
    VIRTIO_CONFIG_S_ACKNOWLEDGE
    VIRTIO_CONFIG_S_DRIVER
    VIRTIO_CONFIG_S_DRIVER_OK
    VIRTIO_CONFIG_S_FEATURES_OK
    VIRTIO_CONFIG_S_DEVICE_NEEDS_RESET
    VIRTIO_CONFIG_S_FAILED)
  (import
    (rnrs (6))
    (loko system fibers)
    (loko system unsafe)
    (only (loko system $host) dma-allocate dma-free))

(define put-mem-s61le put-mem-s61)
(define put-mem-u32le put-mem-u32)
(define put-mem-u16le put-mem-u16)
(define get-mem-s61le get-mem-s61)
(define get-mem-u32le get-mem-u32)
(define get-mem-u16le get-mem-u16)

(define (memory-barrier)
  ;; Not needed on PCs anyway for DMA, or?
  'todo)

;; Device status
(define VIRTIO_CONFIG_S_ACKNOWLEDGE        1)
(define VIRTIO_CONFIG_S_DRIVER             2)
(define VIRTIO_CONFIG_S_DRIVER_OK          4)
(define VIRTIO_CONFIG_S_FEATURES_OK        8)
(define VIRTIO_CONFIG_S_DEVICE_NEEDS_RESET 64)
(define VIRTIO_CONFIG_S_FAILED             128)

(define VIRTIO_MSI_NO_VECTOR #xffff)

;;; Virtqueues

;; Virtqueues have a descriptor table, a ring of available descriptors
;; and a ring of used descriptors. The available descriptors are going
;; to the device and the used ones have been used by the device. The
;; driver writes to the available ring and the device writes to the
;; used ring.

(define sizeof-virtq_desc (+ 8 4 2 2))
(define sizeof-virtq_used_elem (+ 4 4))

(define-record-type virtq
  (sealed #t)
  (fields idx                           ;index of the virtq in the device
          size
          (mutable num-free)
          (mutable free-list)
          (mutable last-seen-used)
          kick-proc
          desc-cookies
          &desc &avail &used)
  (protocol
   (lambda (p)
     (lambda (idx queue-size kick-proc)
       (define (fxalign i alignment)
         (fxand (fx+ i (fx- alignment 1)) (fx- alignment)))
       (define (virtq-used-offset queue-size)
         (fxalign (fx+ (fx* sizeof-virtq_desc queue-size)
                       (fx* 2 (fx+ 3 queue-size)))
                  4096))
       (define (virtq-byte-size queue-size)
         (fx+ (virtq-used-offset queue-size)
              (fxalign (fx+ 6 (fx* sizeof-virtq_used_elem queue-size))
                       4096)))
       (let* ((byte-size (virtq-byte-size queue-size))
              (desc-cookies (make-vector queue-size #f))
              (&desc (dma-allocate byte-size #xFFFFFFFF000))
              (&avail (fx+ &desc (fx* sizeof-virtq_desc queue-size)))
              (&used (fx+ &desc (virtq-used-offset queue-size)))
              ;; Free descriptor indices. TODO: Surely there is a
              ;; smart way to not need this list, but I'm too tired to
              ;; see it.
              (free-list (let lp ((i (fx- queue-size 1)) (idx* '()))
                           (if (eqv? i -1)
                               idx*
                               (lp (fx- i 1) (cons i idx*))))))
         (do ((i 0 (fx+ i 8))) ((fx=? i byte-size))
           (put-mem-s61 (fx+ &desc i) 0))
         (p idx queue-size queue-size free-list 0
            kick-proc desc-cookies
            &desc &avail &used))))))

(define (virtq-enable-interrupts! virtq)
  (virtq-&avail-flags-set! (virtq-&avail virtq) 0))

(define (virtq-disable-interrupts! virtq)
  (virtq-&avail-flags-set! (virtq-&avail virtq) VIRTQ_AVAIL_F_NO_INTERRUPT))

(define (virtq-kick virtq)
  ((virtq-kick-proc virtq)))

(define (virtq-free! virtq)
  ;; FIXME: Free the buffers!
  (dma-free (virtq-&desc virtq)))

(define VIRTQ_DESC_F_NEXT     1)        ;the next field is valid
(define VIRTQ_DESC_F_WRITE    2)        ;write-only
(define VIRTQ_DESC_F_INDIRECT 4)        ;buffer contains descriptors

(define (virtq-desc-set! virtq idx addr len flags next)
  (define (virtq-&desc-set! &desc addr len flags next)
    (put-mem-s61le &desc addr)
    (put-mem-u32le (fx+ &desc 8) len)
    (put-mem-u16le (fx+ &desc 12) flags)
    (put-mem-u16le (fx+ &desc 14) next))
  (assert (fx<? idx (virtq-size virtq)))
  (let ((&desc (fx+ (virtq-&desc virtq) (fx* idx sizeof-virtq_desc))))
    (virtq-&desc-set! &desc addr len flags next)))

(define VIRTQ_AVAIL_F_NO_INTERRUPT 1)

(define (virtq-&avail-flags-set! &avail flags)
  (put-mem-u16le &avail flags))

(define (virtq-avail-idx-set! virtq idx)
  (define (virtq-&avail-idx-set! &avail idx)
    (put-mem-u16le (fx+ &avail 2) idx))
  (virtq-&avail-idx-set! (virtq-&avail virtq) (fxand idx #xffff)))

(define (virtq-avail-idx virtq)
  (get-mem-u16le (fx+ (virtq-&avail virtq) 2)))

(define (virtq-&avail-ring-set! &avail ring-idx desc-idx)
  (put-mem-u16le (fx+ (fx+ &avail 4) (fx* ring-idx 2)) desc-idx))

(define VIRTQ_USED_F_NO_NOTIFY 1)

(define (virtq-&used-flags &used)
  (get-mem-u16le &used))

(define (virtq-&used-idx &used)
  (get-mem-u16le (fx+ &used 2)))

(define (virtq-&used-elem-id &used idx)
  (get-mem-u16le (fx+ (fx+ &used 4) (fx* idx 8))))

(define (virtq-&used-elem-len &used idx)
  (get-mem-u16le (fx+ (fx+ &used (fx+ 4 4)) (fx* idx 8))))

;; Adds a number of readable buffers followed by a number of writable
;; buffers. All of them are associated with a cookie that is returned
;; when the buffer has been used. They are a single chain of buffers.
;; The buf* argument is a list of (physical-address . length) pairs.
(define (virtq-add-buffers! virtq buf* num-in num-out cookie)
  (define (get-descriptor _)
    (let ((free (virtq-free-list virtq)))
      (virtq-free-list-set! virtq (cdr free))
      (car free)))
  (assert cookie)
  (assert (not (null? buf*)))
  (let ((len (length buf*)))
    (cond
      ((fx>? len (virtq-num-free virtq))
       ;; Not enough free descriptors
       #f)
      (else
       (let* ((desc* (map get-descriptor buf*))
              (head-desc (car desc*)))
         ;; Fill in and link the descriptors
         (let lp ((buf* buf*) (desc* desc*) (num-in num-in) (num-out num-out))
           (unless (null? buf*)
             (let ((buf (car buf*))
                   (desc (car desc*))
                   (read? (not (eqv? 0 num-in))))
               (let ((addr (car buf))
                     (len (cdr buf))
                     (flags (fxior (if (null? (cdr desc*)) 0 VIRTQ_DESC_F_NEXT)
                                   (if read? 0 VIRTQ_DESC_F_WRITE)))
                     (next (if (null? (cdr desc*)) 0 (cadr desc*))))
                 (virtq-desc-set! virtq desc addr len flags next)
                 (vector-set! (virtq-desc-cookies virtq) desc cookie)
                 (lp (cdr buf*) (cdr desc*)
                     (if read? (fx- num-in 1) num-in)
                     (if read? num-out (fx- num-out 1)))))))
         (virtq-num-free-set! virtq (fx- (virtq-num-free virtq) len))
         ;; Tell the device about the new buffers. The driver may kick
         ;; the device if it wants to.
         (let ((idx (virtq-avail-idx virtq)))
           (memory-barrier)
           (virtq-&avail-ring-set! (virtq-&avail virtq)
                                   (fxmod idx (virtq-size virtq))
                                   head-desc)
           (virtq-avail-idx-set! virtq (fx+ idx 1))))))))

(define (virtq-get-buffer virtq)
  ;; XXX: Be smart about disabling interrupts and handling multiple buffers
  (let* ((&used (virtq-&used virtq))
         (used-idx (virtq-&used-idx &used)))
    (cond ((fx=? (virtq-last-seen-used virtq) used-idx)
           (values #f #f))
          (else
           (let* ((idx (fxmod (virtq-last-seen-used virtq) (virtq-size virtq)))
                  (desc (virtq-&used-elem-id &used idx))
                  (len (virtq-&used-elem-len &used idx)))
             (virtq-last-seen-used-set! virtq (fxand (fx+ idx 1) #xffff))
             (let ((cookie (vector-ref (virtq-desc-cookies virtq) desc)))
               (vector-set! (virtq-desc-cookies virtq) desc #f)
               (virtq-free-list-set! virtq (cons idx (virtq-free-list virtq)))
               ;; XXX: Could also return the address. Useful?
               (values len cookie)))))))

;;; Virtio device

;; Generic feature flags
(define VIRTIO_F_RING_INDIRECT_DESC 28)
(define VIRTIO_F_RING_EVENT_IDX     29)
(define VIRTIO_F_VERSION_1          32)
(define VIRTIO_F_ACCESS_PLATFORM    33)
(define VIRTIO_F_RING_PACKED        34)
(define VIRTIO_F_IN_ORDER           35)
(define VIRTIO_F_ORDER_PLATFORM     36)
(define VIRTIO_F_SR_IOV             37)
(define VIRTIO_F_NOTIFICATION_DATA  38)

;; Offsets into the configuration registers
(define VIRTIO_PCI_HOST_FEATURES    0)  ;u32
(define VIRTIO_PCI_GUEST_FEATURES   4)  ;u32
(define VIRTIO_PCI_QUEUE_PFN        8)  ;u32
(define VIRTIO_PCI_QUEUE_NUM        12) ;u16, queue size
(define VIRTIO_PCI_QUEUE_SEL        14) ;u16
(define VIRTIO_PCI_QUEUE_NOTIFY     16) ;u16
(define VIRTIO_PCI_STATUS           18) ;u8
(define VIRTIO_PCI_ISR              19) ;u8
(define offsetof-device-config #x14)    ;XXX: changed by MSI-X

(define (virtio-device-status-set! reg-ctl status)
  (put-i/o-u8 (fx+ reg-ctl VIRTIO_PCI_STATUS) status))

(define (virtio-device-status reg-ctl)
  (get-i/o-u8 (fx+ reg-ctl VIRTIO_PCI_STATUS)))

(define (virtio-isr-status! reg-ctl)
  (get-i/o-u8 (fx+ reg-ctl VIRTIO_PCI_ISR)))

(define (virtio-get-config-u8 reg-ctl idx)
  (get-i/o-u8 (fx+ (fx+ offsetof-device-config reg-ctl) idx)))

(define (virtio-get-config-u16 reg-ctl idx)
  (get-i/o-u16 (fx+ (fx+ offsetof-device-config reg-ctl) idx)))

;; Allocates memory for and initiates the virtq. If the queue does not
;; exist on the device then it returns #f.
(define (virtio-allocate-virtq reg-ctl queue-idx)
  (put-i/o-u16 (fx+ reg-ctl VIRTIO_PCI_QUEUE_SEL) queue-idx)
  (let ((queue-size (get-i/o-u16 (fx+ reg-ctl VIRTIO_PCI_QUEUE_NUM))))
    (if (not (eqv? (fxbit-count queue-size) 1))
        #f
        (letrec ((virtq (make-virtq queue-idx queue-size
                                    (lambda ()
                                      (when (eqv? 0 (fxand (virtq-&used-flags (virtq-&used virtq))
                                                           VIRTQ_USED_F_NO_NOTIFY))
                                        (put-i/o-u16 (fx+ reg-ctl VIRTIO_PCI_QUEUE_NOTIFY)
                                                     queue-idx))))))
          (put-i/o-u32 (fx+ reg-ctl VIRTIO_PCI_QUEUE_PFN)
                       (fxdiv (virtq-&desc virtq) 4096))
          virtq))))

(define (virtio-negotiate reg-ctl guest-features)
  (let* ((device-features (get-i/o-u32 (fx+ reg-ctl VIRTIO_PCI_HOST_FEATURES)))
         (common-features (fxand device-features guest-features)))
    (put-i/o-u32 (fx+ reg-ctl VIRTIO_PCI_GUEST_FEATURES) common-features)
    common-features)))
