;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright © 2020 Göran Weinholt <goran@weinholt.se>
;; SPDX-License-Identifier: AGPL-3.0-or-later
#!r6rs

;;; Network interface and packet abstractions

;; XXX: Network packets, by convention, are word aligned in memory.

(library (loko drivers net)
  (export
    make-iface
    iface-type
    iface-rx-ch
    iface-tx-ch
    iface-ctrl-ch
    iface-notify-ch

    make-netpkt netpkt?
    netpkt-header
    netpkt-addrs
    netpkt-bufs
    netpkt-length
    netpkt-copy
    netpkt-free!)
  (import
    (rnrs (6))
    (loko system fibers)
    (only (loko system unsafe) get-mem-u8 put-mem-u8)
    (only (loko system $host) dma-allocate dma-free))

;; A network interface with its channels.
(define-record-type iface
  (sealed #t)
  (fields type
          ;; Receive. Drivers send netpkt records here.
          rx-ch
          ;; Transmit. Put netpkts here to transmit them.
          tx-ch
          ;; Device control. Nothing implemented yet.
          ctrl-ch
          ;; Notifications. Nothing implemented yet.
          notify-ch)
  (protocol
   (lambda (p)
     (lambda (type)
       (p type (make-channel) (make-channel)
          (make-channel) (make-channel))))))

;; Represents a packet as a chain of buffers in physical memory. The
;; first buffer should be the start of the link-layer header.
(define-record-type netpkt
  (sealed #t)
  (fields header                        ;offloading stuff
          addrs                         ;addresses for dma-free
          bufs)                         ;((addr . len) ...)
  (protocol
   (lambda (p)
     (lambda (header addrs bufs)
       (assert (for-all fixnum? addrs))
       (assert (for-all (lambda (x)
                          (and (pair? x)
                               (fixnum? (car x))
                               (fixnum? (cdr x))
                               (fx<=? 0 (cdr x) 65535)))
                        bufs))
       (p header addrs bufs)))))

;; Get the total length of all bytes in the packet's buffers.
(define (netpkt-length pkt)
  (let lp ((bufs (netpkt-bufs pkt))
           (len 0))
    (if (null? bufs)
        len
        (lp (cdr bufs) (fx+ (cdar bufs) len)))))

;; Copy the packet to buffers that are allocated with the given DMA
;; mask. This also moves all data to a single buffer.
(define (netpkt-copy pkt dma-mask)
  (let ((len (netpkt-length pkt)))
    (and (fx=? len (fxbit-field len 0 14))
         (let* ((&buf (dma-allocate len dma-mask))
                (newpkt (make-netpkt (netpkt-header pkt)
                                     (list &buf)
                                     (list (cons &buf len)))))
           ;; Copy each buffer from the old netpkt into the new
           ;; netpkt. This reduces the number of buffers to a
           ;; supported number and moves all the data to 32-bit
           ;; memory.
           (let lp ((buf* (netpkt-bufs pkt))
                    (buf-i 0))
             (unless (null? buf*)
               (do ((&src (caar buf*))
                    (len (cdar buf*))
                    (src-i 0 (fx+ src-i 1))
                    (buf-i buf-i (fx+ buf-i 1)))
                   ((fx>=? src-i len)
                    (lp (cdr buf*) buf-i))
                 (put-mem-u8 (fx+ &buf buf-i)
                             (get-mem-u8 (fx+ &src src-i))))))
           newpkt))))

;; Free a netpkt. If you get a packet from an rx-ch then you're
;; responsible for freeing it. If you put a packet on a tx-ch then the
;; driver is reponsible for freeing it.
(define (netpkt-free! pkt)
  (for-each dma-free (netpkt-addrs pkt))))
