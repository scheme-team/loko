;; -*- mode: scheme; coding: utf-8 -*-
;; SPDX-License-Identifier: MIT
;; Copyright © 2020 Göran Weinholt
#!r6rs

;;; SRFI-174 (POSIX timespecs)

(library (srfi :174)
  (export
    timespec
    timespec?
    timespec-seconds
    timespec-nanoseconds
    inexact->timespec
    timespec->inexact
    timespec=?
    timespec<?
    timespec-hash)
  (import
    (rnrs)
    (loko system time))

(define (timespec s ns)
  (make-time s ns 1))

(define timespec? time?)

(define timespec-seconds time-second)

(define timespec-nanoseconds time-nanosecond)

(define (inexact->timespec inexact)
  (cond
    ((and (flonum? inexact) (not (flfinite? inexact)))
     (assertion-violation 'inexact->timespec
                          "Expected a finite value" inexact))
    (else
     (let* ((s (fltruncate inexact))
            (ns (flabs (fl- inexact s))))
       (make-time (exact s)
                  (exact (round (* 1e9 ns)))
                  1000)))))

(define (timespec->inexact timespec)
  (fl+ (fixnum->flonum (time-second timespec))
       (fl/ (inexact (time-nanosecond timespec)) #i1e9)))

(define (timespec=? timespec1 timespec2)
  (let ((s1 (time-second timespec1))
        (s2 (time-second timespec2))
        (ns1 (time-nanosecond timespec1))
        (ns2 (time-nanosecond timespec2)))
    (and (fx=? s1 s2)
         (fx=? ns1 ns2))))

(define (timespec<? timespec1 timespec2)
  (let ((s1 (time-second timespec1))
        (s2 (time-second timespec2))
        (ns1 (time-nanosecond timespec1))
        (ns2 (time-nanosecond timespec2)))
    (if (fx=? s1 s2)
        (if (fxnegative? s1)
            (fx<? ns2 ns1)
            (fx<? ns1 ns2))
        (fx<? s1 s2))))

(define (timespec-hash timespec)
  (equal-hash `(,(time-second timespec) . ,(time-nanosecond timespec)))))
