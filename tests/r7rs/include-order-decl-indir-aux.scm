;; Copyright © 2020 Göran Weinholt
;; SPDX-License-Identifier: AGPL-3.0-or-later
(include "foo-a.scm" "foo-b.scm")     ;FOO-*
(include-ci "foo-a.scm" "foo-b.scm")  ;foo-*
