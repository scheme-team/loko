Subject: ANN: Loko Scheme version 0.6.9

Loko Scheme 0.6.0 is now available from:

  https://scheme.fail/releases/loko-0.6.0.tar.gz
  https://scheme.fail/releases/loko-0.6.0.tar.gz.sig

The release tarball is signed by the GnuPG key 0xE33E61A2E9B8C3A2.

Loko Scheme 0.6.0 introduces support for R7RS-small. The release
tarballs now include a pre-built compiler and all dependencies needed
for building Loko. See NEWS.md in the distribution for a more detailed
summary of changes.

Loko Scheme is an optimizing Scheme compiler that builds statically
linked binaries for bare metal, Linux and NetBSD/amd64. It supports
the R6RS Scheme and R7RS Scheme standards.

Loko Scheme's web site is <https://scheme.fail>, where you can find
the release tarballs and the manual.

Loko Scheme is available under GNU Affero GPL version 3 or later.
