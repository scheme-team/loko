#!r6rs
;; This file was written by Akku.scm
;; This file is automatically generated and is not a copyrightable work.
(library (akku metadata)
  (export
    main-package-name
    main-package-version
    installed-libraries
    installed-assets)
  (import (only (rnrs) define quote))
  (define main-package-name '"loko-scheme")
  (define main-package-version '"0.6.0")
  (define installed-libraries
    '((laesare reader) (laesare writer) (loko apropos)
      (loko arch amd64 analyzer) (loko arch amd64 codegen)
      (loko arch amd64 disassembler) (loko arch amd64 elf-start)
      (loko arch amd64 lib) (loko arch amd64 lib-gc)
      (loko arch amd64 lib-printer) (loko arch amd64 lib-stacks)
      (loko arch amd64 lib-traps) (loko arch amd64 lib-valgrind)
      (loko arch amd64 linux-asm) (loko arch amd64 linux-init)
      (loko arch amd64 linux-numbers)
      (loko arch amd64 linux-process)
      (loko arch amd64 linux-start)
      (loko arch amd64 linux-syscalls) (loko arch amd64 memory)
      (loko arch amd64 netbsd-asm) (loko arch amd64 netbsd-init)
      (loko arch amd64 netbsd-numbers)
      (loko arch amd64 netbsd-process)
      (loko arch amd64 netbsd-start)
      (loko arch amd64 netbsd-syscalls) (loko arch amd64 objects)
      (loko arch amd64 pc-and-linux-asm)
      (loko arch amd64 pc-ap-boot) (loko arch amd64 pc-asm)
      (loko arch amd64 pc-init) (loko arch amd64 pc-interrupts)
      (loko arch amd64 pc-paging) (loko arch amd64 pc-process)
      (loko arch amd64 pc-segments) (loko arch amd64 pc-start)
      (loko arch amd64 pc-syscalls) (loko arch amd64 playground)
      (loko arch amd64 polyglot-asm) (loko arch amd64 processes)
      (loko arch amd64 prototyping) (loko arch amd64 registers)
      (loko arch amd64 tables) (loko arch amd64 traps)
      (loko arch asm) (loko compiler closure)
      (loko compiler compat) (loko compiler cp0)
      (loko compiler expander) (loko compiler freevar)
      (loko compiler infer) (loko compiler let)
      (loko compiler letrec) (loko compiler loops)
      (loko compiler main) (loko compiler mutation)
      (loko compiler optimize) (loko compiler quasiquote)
      (loko compiler recordize) (loko compiler static)
      (loko compiler values) (loko drivers ata atapi)
      (loko drivers ata core) (loko drivers ata drive)
      (loko drivers ata ide) (loko drivers ata identify)
      (loko drivers keyboard) (loko drivers keymaps)
      (loko drivers mouse) (loko drivers net)
      (loko drivers net eepro100) (loko drivers net rtl8139)
      (loko drivers net tun) (loko drivers net virtio)
      (loko drivers pci) (loko drivers pci roms)
      (loko drivers ps2 core) (loko drivers ps2 i8042)
      (loko drivers ps2 keyboard) (loko drivers ps2 mouse)
      (loko drivers rtc mc146818) (loko drivers scsi block)
      (loko drivers scsi core) (loko drivers storage)
      (loko drivers uart ns8250) (loko drivers usb core)
      (loko drivers usb hid-numbers) (loko drivers usb hub)
      (loko drivers usb mass-storage) (loko drivers usb uhci)
      (loko drivers utils) (loko drivers video bga)
      (loko drivers video vbe) (loko drivers virtio)
      (loko font font-6x13) (loko font font-8x13) (loko match)
      (loko runtime arithmetic) (loko runtime booleans)
      (loko runtime buddy) (loko runtime bytevectors)
      (loko runtime chars) (loko runtime compat)
      (loko runtime conditions) (loko runtime context)
      (loko runtime control) (loko runtime elf)
      (loko runtime enums) (loko runtime equal)
      (loko runtime eval) (loko runtime fibers)
      (loko runtime fixnums) (loko runtime flonums)
      (loko runtime hashtables) (loko runtime init)
      (loko runtime io) (loko runtime io-tc) (loko runtime main)
      (loko runtime pairs) (loko runtime parameters)
      (loko runtime reader) (loko runtime records)
      (loko runtime repl) (loko runtime scheduler)
      (loko runtime sorting) (loko runtime start)
      (loko runtime start-libman) (loko runtime strings)
      (loko runtime symbols) (loko runtime time)
      (loko runtime unicode) (loko runtime unsafe)
      (loko runtime utils) (loko runtime vectors) (loko u8rings)
      (machine-code assembler elf) (machine-code assembler x86)
      (machine-code assembler x86-misc)
      (machine-code assembler x86-operands)
      (machine-code disassembler)
      (machine-code disassembler arm-a64)
      (machine-code disassembler arm-aarch64)
      (machine-code disassembler arm-private)
      (machine-code disassembler i8080)
      (machine-code disassembler m68hc12)
      (machine-code disassembler mips)
      (machine-code disassembler private)
      (machine-code disassembler x86)
      (machine-code disassembler x86-opcodes)
      (machine-code format elf) (pfds bbtrees) (pfds deques)
      (pfds deques naive) (pfds deques private condition)
      (pfds dlists) (pfds fingertrees) (pfds hamts) (pfds heaps)
      (pfds private alists) (pfds private bitwise)
      (pfds private lazy-lists) (pfds private vectors) (pfds psqs)
      (pfds queues) (pfds queues naive)
      (pfds queues private condition) (pfds sequences) (pfds sets)
      (pre-srfi processes) (pre-srfi processes compat)
      (psyntax builders) (psyntax compat) (psyntax config)
      (psyntax expander) (psyntax internal)
      (psyntax library-manager) (scheme base) (scheme case-lambda)
      (scheme char) (scheme complex) (scheme cxr) (scheme eval)
      (scheme file) (scheme inexact) (scheme lazy) (scheme load)
      (scheme process-context) (scheme r5rs) (scheme read)
      (scheme repl) (scheme time) (scheme write) (srfi :174)
      (srfi :198) (srfi :198 private) (srfi :38)
      (srfi :38 with-shared-structure)
      (srfi :98 os-environment-variables) (struct pack)
      (unicode-data)))
  (define installed-assets
    '(((include/resolve ("loko" "runtime") "posixlib.scm")
        ("loko/runtime/posixlib.scm")
        (loko arch amd64 linux-process))
       ((include/resolve ("loko" "runtime") "posixlib.scm")
         ("loko/runtime/posixlib.scm")
         (loko arch amd64 netbsd-process))
       ((include/resolve
          ("loko" "runtime" "unicode")
          "unicode-char-cases.ss")
         ("loko/runtime/unicode/unicode-char-cases.ss")
         (loko runtime unicode))
       ((include/resolve
          ("loko" "runtime" "unicode")
          "unicode-charinfo.ss")
         ("loko/runtime/unicode/unicode-charinfo.ss")
         (loko runtime unicode))
       ((include/resolve ("pre-srfi" "processes") "processlib.scm")
         ("pre-srfi/processes/processlib.scm")
         (pre-srfi processes compat)))))
